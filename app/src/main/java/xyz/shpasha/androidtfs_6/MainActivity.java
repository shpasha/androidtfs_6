package xyz.shpasha.androidtfs_6;

import android.os.HandlerThread;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {

    private TextView tv;
    private Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tv = findViewById(R.id.textView);
        button = findViewById(R.id.button);


        HandlerThread handlerThread = new HandlerThread("HandlerThread");
        handlerThread.start();

        button.setOnClickListener(v -> MyObservable.from(new MyCallable())
                .observeOn(getMainLooper())
                .subscribeOn(handlerThread.getLooper())
                .subscribe(result -> tv.setText(tv.getText() + "\n" + result)));


    }

}

