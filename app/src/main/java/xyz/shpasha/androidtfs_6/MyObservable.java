package xyz.shpasha.androidtfs_6;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import java.util.concurrent.Callable;

public class MyObservable<T> {

    private Callable<Integer> callable;
    private Looper subscribeLooper;
    private Looper observeLooper;

    private MyObservable(Callable callable) {
        this.callable = callable;
    }

    public static MyObservable from(Callable callable) {
        return new MyObservable(callable);
    }

    public MyObservable subscribeOn(Looper looper) {
        this.subscribeLooper = looper;
        return this;
    }

    public MyObservable observeOn(Looper looper) {
        this.observeLooper = looper;
        return this;
    }

    public MyObservable subscribe(final MyCallback callback) {

        if (subscribeLooper == null) {
            /*try {
                T res = (T) callable.call();
                callback.onCall(res);
                return this;
            } catch (Exception e) {
                e.printStackTrace();
            }*/
            subscribeLooper = Looper.myLooper();
        }

        if (observeLooper == null) {
            observeLooper = subscribeLooper;
        }

        Handler handler = new Handler(subscribeLooper);
        handler.post(() -> {
            try {
                Log.d("Completing task on", Thread.currentThread().getName());
                T res = (T) callable.call();
                Handler handler1 = new Handler(observeLooper);
                handler1.post(() -> {
                    Log.d("Handle result to", Thread.currentThread().getName());
                    callback.onCall(res);
                });
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        return this;

    }


}
