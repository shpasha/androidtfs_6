package xyz.shpasha.androidtfs_6;

import java.util.concurrent.Callable;

public class MyCallable<T> implements Callable<T> {

    @Override
    public T call() throws Exception {
        Thread.sleep(5000);
        String res = "Done";
        return (T) res;
    }
}
