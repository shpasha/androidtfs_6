package xyz.shpasha.androidtfs_6;

public interface MyCallback<T> {
    void onCall(T result);
}
